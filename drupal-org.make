api = 2
core = 7.x

;        Modules
;--------------------------
projects[ctools][version] = ""
projects[ctools][subdir] = "contrib"

projects[entity][version] = ""
projects[entity][subdir] = "contrib"

projects[libraries][version] = ""
projects[libraries][subdir] = "contrib"

projects[views][version] = ""
projects[views][subdir] = "contrib"

projects[views_bulk_operations][version] = ""
projects[views_bulk_operations][subdir] = "contrib"

projects[admin_views][version] = ""
projects[admin_views][subdir] = "contrib"

projects[restws][version] = "2.x-dev"
projects[restws][subdir] = "contrib"

projects[token][version] = ""
projects[token][subdir] = "contrib"

projects[ckeditor][version] = ""
projects[ckeditor][subdir] = "contrib"

projects[navbar][version] = ""
projects[navbar][subdir] = "contrib"

projects[module_filter][version] = ""
projects[module_filter][subdir] = "contrib"

projects[entityreference][version] = ""
projects[entityreference][subdir] = "contrib"

projects[migrate][version] = ""
projects[migrate][subdir] = "contrib"

projects[email][version] = ""
projects[email][subdir] = "contrib"

projects[date][version] = ""
projects[date][subdir] = "contrib"

projects[link][version] = ""
projects[link][subdir] = "contrib"

projects[phone][version] = ""
projects[phone][subdir] = "contrib"

projects[picture][version] = ""
projects[picture][subdir] = "contrib"

projects[jquery_update][version] = ""
projects[jquery_update][subdir] = "contrib"

projects[edit][version] = "1.x-dev"
projects[edit][subdir] = "contrib"

;        Themes
;--------------------------
projects[responsive_bartik][type] = "theme"
projects[responsive_bartik][version] = "1.0-beta2"

;        Libraries
;--------------------------
; CKEditor
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][type] = "library"
libraries[ckeditor][destination] = "libraries"
libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.4.0/ckeditor_4.4.0_full.tar.gz"

; underscore
libraries[underscore][type] = "library"
libraries[underscore][directory_name] = "underscore"
libraries[underscore][destination] = "libraries"
libraries[underscore][download][type] = "get"
libraries[underscore][download][url] = "https://github.com/jashkenas/underscore/archive/1.6.0.zip"

; backbone
libraries[backbone][type] = "library"
libraries[backbone][directory_name] = "backbone"
libraries[backbone][destination] = "libraries"
libraries[backbone][download][type] = "get"
libraries[backbone][download][url] = "https://github.com/jashkenas/backbone/archive/1.1.2.zip"

; Modernizr
libraries[modernizr][type] = "library"
libraries[modernizr][directory_name] = "modernizr"
libraries[modernizr][destination] = "libraries"
libraries[modernizr][download][type] = "get"
libraries[modernizr][download][url] = "https://github.com/Modernizr/Modernizr/archive/v2.7.2.zip"

