api = 2
core = 7.x

; Include Drupal core and any core patches.
includes[] = drupal-org-core.make

; PreD8 install profile
projects[pred8][type] = "profile"
projects[pred8][download][type] = git
projects[pred8][download][branch] = 7.x-1.x
